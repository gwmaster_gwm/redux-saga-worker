const wpr = 'W_'
export const WORKERS = {
  ALL: `${wpr}ALL`,
  WORKER: `${wpr}W`,
  MAIN: `${wpr}M`
}

const pr = 'REDUX_WORKER_ACTIONS_'
export const REDUX_WORKER_ACTIONS = {
  INIT: `${pr}INIT`
}