import { WorkerManager } from './workerTools'
import { WORKERS } from './worker.const'
import {deserialize as _deserialize,serialize as _serialize} from './serializeDesizialize'

function createSagaWorkerMiddleware(){
  let rootStore;
  let myWorkerManager;
  let sendToWorkerName;
  const middleware = store => next => action => {
    const {sendTo = false , receiver = false} = action
    if(sendTo == sendToWorkerName || sendTo == WORKERS.ALL){
      myWorkerManager.callFunction('runReduxAction', {...action , receiver : sendToWorkerName})
      if(sendTo == WORKERS.ALL ){
        next(action)
      }
    }else{
      next(action)
    }
  }
  const run = (config) => {
      const {
        store,
        storeWorker,
        workerName,
        initConfig,
        onInit,
        serialize,
        deserialize
      } = {
        workerName : WORKERS.WORKER,
        initConfig : {},
        onInit : () => {},
        serialize : _serialize,
        deserialize : _deserialize,
        ...config
      }

      rootStore = store
      sendToWorkerName = workerName
      //worker
      myWorkerManager = new WorkerManager(storeWorker,serialize,deserialize, workerName, initConfig)
      myWorkerManager.init = onInit
      myWorkerManager.runReduxAction = (action) => {
        const defaultAction = function () {
          return action
        }
        store.dispatch(defaultAction())
      }
  }
  return [middleware , run]
}
export default createSagaWorkerMiddleware