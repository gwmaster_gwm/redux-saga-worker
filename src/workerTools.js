import {serialize,deserialize} from './serializeDesizialize'

export class WorkerManager {
  constructor (worker,serialize,deserialize, ...params) {
    this._worker = worker
    this.serialize = serialize;
    this.deserialize = deserialize;
    this._worker.addEventListener('message', this.onMessage)
    this.callFunction('_init', params)
  }

  onMessage = (event) => {
    const {fName, data = [] } = this.deserialize(event.data)
    try {
      this[fName].apply(this, data)
    } catch (e) {
      console.error('parent worker function failed ' + e)
    }
  }
  callFunction = (fName, ...data) => {
    this._worker.postMessage(this.serialize({ fName, data }))
  }
}

export class ParentManager {
  constructor (store , workerName , onInit , serialize,deserialize) {
    this.store = store;
    this.workerName = workerName;
    this.onInit = onInit;
    this.serialize = serialize;
    this.deserialize = deserialize;
    self.addEventListener('message', this.onMessage)
  }
  runReduxAction = (action) => {
    const defaultAction = function () {
      return action
    }
    this.store.dispatch(defaultAction())
  }
  _init = (event) =>{
    this.onInit(event[1])
    self.postMessage(this.serialize({ fName: 'init' , data : [{workerName : this.workerName}] }))
  }
  callFunction = (fName, ...data) => {
    self.postMessage(this.serialize({ fName, data }))
  }
  onMessage = (event) => {
    const {  fName, data = [] } = this.deserialize(event.data)
    try {
      this[fName].apply(this.classObj, data)
    } catch (e) {
      console.error('worker function failed ' + e)
    }
  }

}