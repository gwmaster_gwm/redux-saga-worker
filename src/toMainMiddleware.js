import { ParentManager } from './workerTools'

import { WORKERS } from './worker.const'

function createSagaWorkerMiddleware(){
  let rootStore;
  let myWorkerManager;
  let sendToWorkerName;
  const middleware = store => next => action => {
    const {sendTo = false , receiver = false} = action
    if(sendTo != false && receiver == false){
      myWorkerManager.callFunction('runReduxAction', {...action , receiver : sendToWorkerName})
    }
    if(sendTo == false || sendTo == WORKERS.ALL || receiver != false) {
      next(action); // execute action
    }
  }
  const run = (store, onInit , serialize,deserialize , workerName = WORKERS.MAIN) => {
      sendToWorkerName = workerName
      rootStore = store
      myWorkerManager = new ParentManager(store , workerName , onInit , serialize,deserialize)
  }
  return [middleware , run]
}
export default createSagaWorkerMiddleware