import { applyMiddleware, combineReducers, createStore } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { toMainMiddleware } from './index'
import { deserialize as _deserialize, serialize as _serialize } from './serializeDesizialize'

const workerStore = (config) => {
  const {
    saga,
    reducers,
    onInit,
    middleware,
    serialize,
    deserialize
  } = {
    onInit : () => {},
    middleware : [],
    serialize : _serialize,
    deserialize : _deserialize,
    ...config
  }
  const sagaMiddleware = createSagaMiddleware()
  const [workerMiddleware,initWorkerMiddleware] = new toMainMiddleware()

  let combinedReducers = combineReducers(reducers)
  const _middleware = [
    workerMiddleware, // must be first
    sagaMiddleware,
    ...middleware
  ]
  const store = createStore(
    combinedReducers,
    applyMiddleware(..._middleware)
  )
  // then run the saga
  sagaMiddleware.run(saga)
  // then init worker
  initWorkerMiddleware(store , onInit , serialize,deserialize )
}

export default workerStore