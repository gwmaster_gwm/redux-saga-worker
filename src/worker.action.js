import { REDUX_WORKER_ACTIONS } from './worker.const'

function init (name) {
  return {
    type: REDUX_WORKER_ACTIONS.INIT,
    payload: name
  }
}
export const Actions = {
  init,
}