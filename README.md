# Redux Saga Worker Middleware


```
npm i redux-saga-worker
```

## Intro

Enrich Redux store actions:
The middleware enable users to perform complex tasks in the client side at real-time with high performance.

If you have an application that processes big data in real-time or handle millions of events in client side, 
then this middleware will allow the application to be easily scaled without the worry of managing web worker threads and communication.

The middleware enriches the standard redux store actions by allowing the user to specify actions to be be send directly to web workers or main thread.
By defining the "sendTo" attribute in the action, the action will perform in the main thread or new web worker.
then, information is saved to the standard redux store and will be accessible to all of the components in the application.

## Demo
A minimal example can be found as below:


https://github.com/gwmaster/redux-saga-worker-example

# Open in browser
[Live Example Click here](http://gwmaster.byethost32.com/redux-saga-worker-example/)

Check out the demo:
![](http://g.recordit.co/hqYTQFzZZJ.gif)

## License
MIT
